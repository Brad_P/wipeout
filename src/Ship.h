#pragma once

//System

//Ogre
#include "Ogre.h"

//Bullet
#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"

//Project


using namespace Ogre;

//The Ship class is parent to all ships. This contains the things every ship must have
class Ship {

public:


	//All ships must have an update method
	virtual void update(float) = 0;

	virtual SceneNode* getSceneNode() = 0;

	virtual btRigidBody* getRigidBody() = 0;

	virtual void setPosition(btVector3, btQuaternion) = 0;
};