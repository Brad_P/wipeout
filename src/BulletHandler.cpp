#pragma once
#include "Ogre.h"

#include "BulletHandler.h"


//This is used to handle the physics objects in the game
//eg adding, removing and updating

BulletHandler::BulletHandler()
{
	dynamicsWorld = NULL;
	init();
}

BulletHandler::~BulletHandler()
{
	/*cleanup in the reverse order of creation/initialization
 -----cleanup_start----
 remove the rigidbodies from the dynamics world and delete them
 removed/delete constraints */
	for (int i = dynamicsWorld->getNumConstraints() - 1; i >= 0; i--)
	{
		btTypedConstraint* constraint = dynamicsWorld->getConstraint(i);
		dynamicsWorld->removeConstraint(constraint);
		delete constraint;
	}

	for (int i = dynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i--)
	{
		btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[i];
		btRigidBody* body = btRigidBody::upcast(obj);

		if (body && body->getMotionState())
		{
			delete body->getMotionState();
		}

		dynamicsWorld->removeCollisionObject(obj);
		delete obj;
	}

	//delete collision shapes
	for (int j = 0; j < collisionShapes.size(); j++)
	{
		btCollisionShape* shape = collisionShapes[j];
		collisionShapes[j] = 0;
		delete shape;
	}

	//delete dynamics world
	delete dynamicsWorld;

	//delete solver
	delete solver;

	//delete broadphase
	delete overlappingPairCache;

	//delete dispatcher
	delete dispatcher;

	delete collisionConfiguration;

	//next line is optional: it will be cleared by the destructor when the array goes out of scope
	collisionShapes.clear();
}

void BulletHandler::init()
{
	//collision configuration contains default setup for memory, collision setup. Advanced users can create their own configuration.
	collisionConfiguration = new btDefaultCollisionConfiguration();

	//use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
	dispatcher = new btCollisionDispatcher(collisionConfiguration);

	//btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
	overlappingPairCache = new btDbvtBroadphase();

	//the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
	solver = new btSequentialImpulseConstraintSolver();

	dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);

	dynamicsWorld->setGravity(btVector3(0, -10, 0));

}

void BulletHandler::update(float deltaTime)
{

	if (this->dynamicsWorld != NULL)
	{

		dynamicsWorld->stepSimulation(deltaTime, 5);
	}
}

//Cast a ray directly down from a position, and return the distance to the first object hit.
float BulletHandler::raycastDownHitDist(btVector3 position) {

	//The point the ray travels to (directly below player)
	btVector3 rayToward = position - btVector3(0, 10000, 0);

	btCollisionWorld::ClosestRayResultCallback closestResults(position, rayToward);
	
	dynamicsWorld->rayTest(position, rayToward, closestResults);

	if (closestResults.hasHit()) {
		btVector3 hitPosition = closestResults.m_hitPointWorld;

		float distance = position.getY() - hitPosition.getY();

		return distance;
	}

	//no hit
	return 10000.0f;
}
