#pragma once

//System
#include <vector>

//Ogre
#include "Ogre.h"

//Ogre-Bullet helpers
#include "BtOgreExtras.h"
#include "BtOgreGP.h"
#include "BtOgrePG.h"

//Bullet
#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"

//Project
#include "BulletHandler.h"
#include "Ship.h"
#include "Player.h"
#include "NPCRacer.h"

#define FACTORY_SHIP_TYPE_PLAYER 0
#define FACTORY_SHIP_TYPE_NPC 1

using namespace Ogre;
using namespace btOgre;

class ShipFactory {

public:

	//The factory must know the scene manager to create nodes.
	//Must also know about bullet handler to add the rigid body and collision shape to.
	ShipFactory(SceneManager*, BulletHandler*);
	~ShipFactory();

	//Receives mesh file path and the type of ship to generate
	//Also needs keystate for if the ship is a player
	Ship* createShip(String, int, Keystate, Vector3);

	void cleanUpShips();


	//This contains all of the ships that have been generated
	std::vector<Ship*> ships;

private:

	btCollisionShape* setupBoundingBox(SceneNode*, Vector3);
	btRigidBody* setupDynamicRigidBody(SceneNode*, btCollisionShape*);

	SceneManager* scnMgr;
	BulletHandler* bulletHandler;

	bool playerCreated = false;


};