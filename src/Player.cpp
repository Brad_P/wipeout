#pragma once 

#include "Player.h"

Player::Player(SceneNode* scnNode, btRigidBody* rb, Keystate k, BulletHandler* bHandler)
{

	playerSceneNode = scnNode;


	playerRigidBody = rb;
	keys = k;
	bulletHandler = bHandler;

	setupHover();
}

Player::~Player()
{
}

//Create nodes on each corner for stabilisation and hovering
void Player::setupHover() {

	frontLeftHoverNode = playerSceneNode->createChildSceneNode();
	frontRightHoverNode = playerSceneNode->createChildSceneNode();
	backLeftHoverNode = playerSceneNode->createChildSceneNode();
	backRightHoverNode = playerSceneNode->createChildSceneNode();

	const AxisAlignedBox& playerAABB = playerSceneNode->_getWorldAABB();
	Vector3 AABBSize = playerAABB.getSize();

	frontLeftHoverNode->translate(Vector3(-AABBSize.x / 2, AABBSize.y/2, -AABBSize.z / 2), Node::TS_PARENT);
	frontRightHoverNode->translate(Vector3(AABBSize.x / 2, AABBSize.y/2, -AABBSize.z / 2), Node::TS_PARENT);
	backLeftHoverNode->translate(Vector3(-AABBSize.x / 2, AABBSize.y/2 , AABBSize.z / 2), Node::TS_PARENT);
	backRightHoverNode->translate(Vector3(AABBSize.x / 2, AABBSize.y/2 , AABBSize.z / 2), Node::TS_PARENT);

	hoverNodes.push_back(frontLeftHoverNode);
	hoverNodes.push_back(frontRightHoverNode);
	hoverNodes.push_back(backLeftHoverNode);
	hoverNodes.push_back(backRightHoverNode);
}


void Player::update(float deltaTime)
{
	calcHover();
	calcMovement();

	//std::cout << "x: " << playerSceneNode->getPosition().x << ", y: " << playerSceneNode->getPosition().y << ", z: " << playerSceneNode->getPosition().z << std::endl;
}

void Player::calcHover() {

	btVector3 baseForce;

	float playerDist = bulletHandler->raycastDownHitDist(vec3ToBT(playerSceneNode->getPosition()));
	if (playerDist < 100) {
		float forceY = 2.5f;
		//0 means at hover height. < 0 means lower than target hover height
		float height = playerDist - HOVER_HEIGHT;

		forceY = forceY - (height * multiplier);
		baseForce = btVector3(0.0f, forceY, 0.0f);
	}
	else {
		baseForce = btVector3(0.0f, 0.0f, 0.0f);
	}

	//loop through each node
	for (int i = 0; i < hoverNodes.size(); i++) {

		btVector3 force;
		//First, calculate distance from ground of each node
		float dist = bulletHandler->raycastDownHitDist(vec3ToBT(hoverNodes[i]->_getDerivedPosition()));

		//if dist < hover height, add more force. Else no force added
		if (dist < 100) {
			float forceYHover = 0.0f;
			//0 means at hover height. < 0 means lower than target hover height
			float height = dist - (playerDist + hoverNodes[i]->getPosition().y);

			if (height > 0)
			{
				forceYHover = forceYHover - (height * hoverMultiplierAbove);
			}
			else
			{
				forceYHover = forceYHover - (height * hoverMultiplierBelow);
			}
			force = btVector3(0.0f, forceYHover, 0.0f);
		}
		else {
			force = btVector3(0.0f, 0.0f, 0.0f);
		}

		btVector3 position = vec3ToBT(hoverNodes[i]->getPosition());
		playerRigidBody->applyForce(force + baseForce, position);
	}
}


void Player::calcMovement()
{		

	btVector3 pos = vec3ToBT(playerSceneNode->getPosition());
	btQuaternion rot = ogreQuatToBT(playerSceneNode->getOrientation());

	//Individual IFs, since multiple of these may apply
	if (keys & Keys::Forward) {

		btVector3 force;
		Quaternion currRot = playerSceneNode->getOrientation();

		Vector4 forwards = Vector4(0, 0, speed, 1);

		Vector4 realForwards = currRot * forwards;

		Vector3 realForwards3;
		realForwards3.x = realForwards.x;
		realForwards3.y = realForwards.y;
		realForwards3.z = realForwards.z;

		force = vec3ToBT(realForwards3);

		playerRigidBody->applyCentralForce(force);

	}
	if (keys & Keys::Backward) {

		btVector3 force;
		Quaternion currRot = playerSceneNode->getOrientation();

		Vector4 forwards = Vector4(0, 0, -speed, 1);

		Vector4 realForwards = currRot * forwards;

		Vector3 realForwards3;
		realForwards3.x = realForwards.x;
		realForwards3.y = realForwards.y;
		realForwards3.z = realForwards.z;

		force = vec3ToBT(realForwards3);

		playerRigidBody->applyCentralForce(force);

		playerRigidBody->applyCentralForce(force);
	}
	if (keys & Keys::Left) {

		btVector3 torque(0.0f, turnSpeed, 0.0f);
		playerRigidBody->applyTorque(torque);
	}
	if (keys & Keys::Right) {

		btVector3 torque(0.0f, -turnSpeed, 0.0f);
		playerRigidBody->applyTorque(torque);
	}

}

//Set the new world position and rotation (Only used for teleport -- not for regular movement)
void Player::setPosition(btVector3 position, btQuaternion rotation)
{
	btTransform transform;

	transform.setOrigin(position);
	transform.setRotation(rotation);

	//Must set the world transform of the rigid body and of the motion state
	playerRigidBody->setWorldTransform(transform);
	playerRigidBody->getMotionState()->setWorldTransform(transform);
}

