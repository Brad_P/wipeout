#pragma once

#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"


class BulletHandler {

public:

	BulletHandler();
	~BulletHandler();	
	
	void init();
	void update(float);
	float raycastDownHitDist(btVector3);

	//keep track of the shapes, we release memory at exit.
	//make sure to re-use collision shapes among rigid bodies whenever possible!
	btAlignedObjectArray<btCollisionShape*> collisionShapes;

	btDiscreteDynamicsWorld* dynamicsWorld;

private:

	//collision configuration.
	btDefaultCollisionConfiguration* collisionConfiguration;

	//use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
	btCollisionDispatcher* dispatcher;

	//btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
	btBroadphaseInterface* overlappingPairCache;

	//the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
	btSequentialImpulseConstraintSolver* solver;


};