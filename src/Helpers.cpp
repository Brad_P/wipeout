#include "Helpers.h"

//Create a quaternion from 3 radians
Quaternion genQuaternion(Radian x, Radian y, Radian z) {

	Quaternion qX;
	qX.FromAngleAxis(x, Vector3(1, 0, 0));
	Quaternion qY;
	qY.FromAngleAxis(y, Vector3(0, 1, 0));
	Quaternion qZ;
	qZ.FromAngleAxis(z, Vector3(0, 0, 1));

	return qX * qY * qZ;
}

//Convert Ogre quaternion to Bullet Quaternion
btQuaternion ogreQuatToBT(Quaternion q) {

	btQuaternion quat;
	quat.setX(q.x);
	quat.setY(q.y);
	quat.setZ(q.z);
	quat.setW(q.w);

	return quat;
}

//Convert Ogre Vector3 to Bullet Vector3
btVector3 vec3ToBT(Vector3 vec) {

	btVector3 btVec;
	btVec.setX(vec.x);
	btVec.setY(vec.y);
	btVec.setZ(vec.z);

	return btVec;
}