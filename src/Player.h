#pragma once

//System
#include <iostream>

//Ogre

//Bullet

//Project
#include "BulletHandler.h"
#include "Ship.h"
#include "Keystates.h"
#include "Helpers.h"

//The player is a ship
class Player : public Ship {

public:

	//Player needs to know about the sceneNode, and the rigid body for applying forces
	Player(SceneNode*, btRigidBody*, Keystate, BulletHandler*);

	~Player();

	void update(float);

	SceneNode* getSceneNode() { return playerSceneNode; }
	btRigidBody* getRigidBody() { return playerRigidBody; }
	void setPosition(btVector3, btQuaternion);

	//When game updates the keystate, player's keystates must be updated to be the same
	void updateKeystate(Keystate k) { keys = k; }

private:

	void setupHover();
	void calcHover();
	void calcMovement();

	BulletHandler* bulletHandler;

	SceneNode* playerSceneNode;
	SceneNode* frontLeftHoverNode;
	SceneNode* frontRightHoverNode;
	SceneNode* backLeftHoverNode;
	SceneNode* backRightHoverNode;

	std::vector<SceneNode*> hoverNodes;

	btRigidBody* playerRigidBody;
	Keystate keys;

	const float HOVER_HEIGHT = 5.0f;
	float multiplier = 1.0f;
	float hoverMultiplierAbove = 0.35f;
	float hoverMultiplierBelow = 0.35f;

	float turnSpeed = 150.0f;
	float speed = 160.0f;
};
