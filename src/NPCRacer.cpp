#include "NPCRacer.h"

NPCRacer::NPCRacer(SceneNode* scnNode, btRigidBody* rb, BulletHandler* bHandler)
{

	mySceneNode = scnNode;
	myRigidBody = rb;

	bulletHandler = bHandler;
}

NPCRacer::~NPCRacer()
{
}

void NPCRacer::update(float deltaTime)
{
	targetNode = TRACK_PATH_POSITIONS[nodeIndex];

	//First check if the position is within triggering range of the target node
	if (isAtNode(TRACK_PATH_POSITIONS[nodeIndex], mySceneNode->getPosition())) {
		//is at node, so switch to next node
		nodeIndex++;
		
		if (nodeIndex > TRACK_PATH_POSITIONS.size() - 1) {

			//If new node is out of bounds, set back to start
			nodeIndex = 0;
		}
		//Set target to next node
		targetNode = TRACK_PATH_POSITIONS[nodeIndex];
	}

	//Now do movement
	Vector3 difference = TRACK_PATH_POSITIONS[nodeIndex] - mySceneNode->getPosition();
	
	float rotToTarget = atan2(difference.x, difference.z);

	Quaternion newRot = genQuaternion(Radian(0.0), (Radian)rotToTarget, Radian(0.0));

	difference.normalise();

	Vector3 movement(difference * speed);

	setPosition(vec3ToBT(mySceneNode->getPosition() + movement), ogreQuatToBT(newRot));
}


void NPCRacer::setPosition(btVector3 position, btQuaternion rotation) {

	btTransform transform;

	transform.setOrigin(position);
	transform.setRotation(rotation);

	myRigidBody->setWorldTransform(transform);
	myRigidBody->getMotionState()->setWorldTransform(transform);

}

//Returns true if player is within a radius of the node
bool NPCRacer::isAtNode(Vector3 nodePosition, Vector3 position)
{
	//This calculates the distance between the two nodes
	float length = nodePosition.distance(position);
	//std::cout << length << std::endl;
	if (length < NODE_HIT_RADIUS) {
		
		//position is within radius of node
		return true;
	}
	else {

		return false;
	}
}
