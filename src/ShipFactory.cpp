#pragma once

#include "ShipFactory.h"

ShipFactory::ShipFactory(SceneManager* scm, BulletHandler* bHandler)
{
	scnMgr = scm;
	bulletHandler = bHandler;
}

ShipFactory::~ShipFactory()
{
}

Ship* ShipFactory::createShip(String fp, int shipType, Keystate keys, Vector3 scale)
{
	//Create an Entity with the filepath of the mesh
	String filepath = fp;
	Entity* shipEntity = scnMgr->createEntity(fp);

	shipEntity->setCastShadows(true);
	shipEntity->getWorldBoundingBox();

	SceneNode* shipSceneNode;
	//Link the mesh to a scene node
	if (!playerCreated) {
		shipSceneNode = scnMgr->getRootSceneNode()->createChildSceneNode("player");
		shipSceneNode->attachObject(shipEntity);
		playerCreated = true;
	}
	else {
		shipSceneNode = scnMgr->getRootSceneNode()->createChildSceneNode();
		shipSceneNode->attachObject(shipEntity);

	}



	//Get the collision shape and rigid body
	btCollisionShape* shipCollShape = setupBoundingBox(shipSceneNode, scale);
	btRigidBody* shipRigidBody = setupDynamicRigidBody(shipSceneNode, shipCollShape);

	//Push those to the bullet object handler
	bulletHandler->collisionShapes.push_back(shipCollShape);
	bulletHandler->dynamicsWorld->addRigidBody(shipRigidBody);

	//Create our ship as the specified type of ship
	//Then add the ship to list of all ships
	Ship* thisShip;
	switch (shipType) {

	case FACTORY_SHIP_TYPE_PLAYER:
		thisShip = new Player(shipSceneNode, shipRigidBody, keys, bulletHandler);
		ships.push_back(thisShip);
		break;

	case FACTORY_SHIP_TYPE_NPC:
		thisShip = new NPCRacer(shipSceneNode, shipRigidBody, bulletHandler);
		ships.push_back(thisShip);
		break;

	default:
		//incorrect type specified

		return nullptr;
		break;

	}
	return thisShip;
}


btCollisionShape* ShipFactory::setupBoundingBox(SceneNode* shipSceneNode, Vector3 scale)
{
	//Default shape axis to be same way up as in modelling project
	Vector3 axis(0.0, 1.0, 0.0);
	axis.normalise();
	//No rotational angle
	Radian rads(0.0);
	//Create quaternion from these
	Quaternion quat(rads, axis);
	shipSceneNode->setScale(scale.x, scale.y, scale.z);
	//get the bounds of the shape
	shipSceneNode->_updateBounds();
	const AxisAlignedBox& shipAABB = shipSceneNode->_getWorldAABB();
	//shipSceneNode->showBoundingBox(true);

	//Set transformations

	shipSceneNode->setOrientation(quat);
	shipSceneNode->setPosition(0, 2, 0);

	//Create a collision shape from the mesh's dimensions and return it
	Vector3 meshAABB = shipAABB.getSize();
	btCollisionShape* cShape = new btBoxShape(btVector3(meshAABB.x / 2.0f, meshAABB.y / 2.0f, meshAABB.z / 2.0f));

	return cShape;
}

btRigidBody* ShipFactory::setupDynamicRigidBody(SceneNode* shipSceneNode, btCollisionShape* shipCollShape)
{
	//Set initial transforms of the dynamic rigid body
	btTransform startTransform;
	startTransform.setIdentity();

	//Initialize position and rotation
	Vector3 pos = shipSceneNode->_getDerivedPosition();
	startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

	Quaternion rot = shipSceneNode->_getDerivedOrientation();
	startTransform.setRotation(btQuaternion(rot.x, rot.y, rot.z, rot.w));

	//Set the mass of the rigid body
	btScalar mass(1.0f);

	//rb is dynamic as long as mass in non-zero
	bool isDynamic = (mass != 0.0f);

	//init inertia. If object is not dynamic, the statement will not pass therefore inertia remains zeroed
	btVector3 localInertia(0, 0, 0);
	if (isDynamic) {

		shipCollShape->calculateLocalInertia(mass, localInertia);
	}

	//This updates the scene node accordingly with the rigid body.
	RigidBodyState* motionState = new RigidBodyState(shipSceneNode);

	//Construct the rigid body using the calculated information above
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, motionState, shipCollShape, localInertia);
	btRigidBody* body = new btRigidBody(rbInfo);

	body->setRestitution(0);
	//Link the rigid body to ogre's scene node
	body->setUserPointer(shipSceneNode);
	body->setDamping(0.65, 0.9);
	return body;
}



void ShipFactory::cleanUpShips()
{
	for each (Ship* s in ships)
	{
		delete s;
	}
}


