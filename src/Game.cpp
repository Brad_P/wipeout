/*-------------------------------------------------------------------------
Significant portions of this project are based on the Ogre Tutorials
- https://ogrecave.github.io/ogre/api/1.10/tutorials.html
Copyright (c) 2000-2013 Torus Knot Software Ltd

Manual generation of meshes from here:
- http://wiki.ogre3d.org/Generating+A+Mesh

*/

#include <exception>

#include "Game.h"

Game::Game() : ApplicationContext("Wipeout Project")
{
}


Game::~Game()
{
 
}


void Game::setup()
{
    // do not forget to call the base first
    ApplicationContext::setup();
    addInputListener(this);
    // get a pointer to the already created root
	Root* root = getRoot();

	//Get a pointer to the window for resizing and positioning
	mainWindow = getRenderWindow();
	mainWindow->resize(1920, 1080);
	mainWindow->reposition(0, 0);


    scnMgr = root->createSceneManager();

    //register our scene with the RTSS
    RTShader::ShaderGenerator* shadergen = RTShader::ShaderGenerator::getSingletonPtr();
    shadergen->addSceneManager(scnMgr);

	//Setup all of our seperate components
    setupBullet();

	setupShipFactory();

	setupPlayer();

	setupRacers();

	setupTrack();

    setupCam();

    setupLights();

}

void Game::setupBullet()
{
	//Create our handler to deal with adding, removing and updating physics objects
	bulletHandler = new BulletHandler();
	bulletHandler->init();
}

void Game::setupShipFactory()
{
	shipFactory = new ShipFactory(scnMgr, bulletHandler);
}


void Game::setupWorld() {

	world = new World();

}


void Game::setupPlayer() {

	//Vector3 playerScale(0.04, 0.04, 0.04);	
	Vector3 playerScale(0.5, 0.5, 0.5);
	//Create our player ship using the ship factory.
	//Pass in the filepath of the mesh, and the type of ship to create (player).
	player = (Player*)shipFactory->createShip("oblongship.mesh", FACTORY_SHIP_TYPE_PLAYER, keys, playerScale);
	//Add the player to the list of racers
	racers.push_back(player);
}

//Generates NPC racers, and sets the start position of all racers (including player)
void Game::setupRacers() {

	Vector3 npcScale(0.5, 0.5, 0.5);
//GEN NPC RACERS
	int numNPCRacers = 1;
	for (int i = 0; i < numNPCRacers; i++) {

		//Generate a new NPC racer using ship factory
		NPCRacer* racer = (NPCRacer*)shipFactory->createShip("oblongship.mesh", FACTORY_SHIP_TYPE_NPC, keys, npcScale);
		racers.push_back(racer);	//Add racer to list of ships in game

	} //Gen next NPC racer


//SET START POSITIONS
	//Seed random function
	std::srand((unsigned)clock());

	//Copy the racers vector, then randomly shuffle all of them in the new vector
	std::vector<Ship*> racersShuffle = racers;
	std::random_shuffle(racersShuffle.begin(), racersShuffle.end());
	
	//Now set the position of each of the shuffled racers to a starting position
	for (int i = 0; i < racersShuffle.size(); i++) {

		//set initial position and rotation of the ships.
		btVector3 position(START_POSITIONS[i].x, START_POSITIONS[i].y, START_POSITIONS[i].z);
		btQuaternion rot = ogreQuatToBT(genQuaternion(Radian(0.0), Radian(M_PI), Radian(0.0)));

		racersShuffle[i]->setPosition(position, rot);
	}
	//We're now done with the shuffled ships so call destructor
	racersShuffle.~vector();
}

//Setup the trimesh track
void Game::setupTrack() {

	Entity* trackEntity = scnMgr->createEntity("track.mesh");
	SceneNode* trackSceneNode = scnMgr->getRootSceneNode()->createChildSceneNode();
	trackSceneNode->attachObject(trackEntity);
	trackSceneNode->setScale(5, 5, 5);
	//triMeshPlaneSceneNode->showBoundingBox(true);

	//Make the collision shape
	StaticMeshToShapeConverter* conv = new StaticMeshToShapeConverter(trackEntity);
	btBvhTriangleMeshShape* triMesh = conv->createTrimesh();
	bulletHandler->collisionShapes.push_back(triMesh);


	//SETUP RIGID BODY

	btTransform groundTransform;
	groundTransform.setIdentity();

	Vector3 pos = trackSceneNode->_getDerivedPosition();


	groundTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

	Quaternion quat2 = trackSceneNode->_getDerivedOrientation();
	groundTransform.setRotation(btQuaternion(quat2.x, quat2.y, quat2.z, quat2.w));


	btScalar mass(0.0f);
	bool isDynamic = (mass != 0.f);

	btVector3 localInertia(0, 0, 0);
	if (isDynamic)
		triMesh->calculateLocalInertia(mass, localInertia);

	RigidBodyState* myMotionState = new RigidBodyState(trackSceneNode);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, triMesh, localInertia);
	btRigidBody* body = new btRigidBody(rbInfo);

	//add the body to the dynamics world
	bulletHandler->dynamicsWorld->addRigidBody(body);

	body->setUserPointer(trackSceneNode);
}



void Game::setupCam()
{
	
    // Create Camera
    Camera* cam = scnMgr->createCamera("myCam");

	SceneNode* camNode = scnMgr->getRootSceneNode()->createChildSceneNode();
	camNode->attachObject(cam);

	//Pass in its own scene node, the player scene node, the looking node , and the OGRE camera
	playerCam = new PlayerCamera(camNode, player->getSceneNode());
	playerCam->initViewport(mainWindow, cam);
	playerCam->setupCamera(scnMgr);
}


bool Game::frameStarted(const Ogre::FrameEvent &evt)
{	
	//Be sure to call base class - otherwise events are not polled.
	ApplicationContext::frameStarted(evt);
	float deltaTime = (float)evt.timeSinceLastFrame;


	//UPDATE GAME OBJECTS

	//All racers (including player)
	for (int i = 0; i < racers.size(); i++) {

		if (racers[i] != nullptr) {

			racers[i]->update(deltaTime);
		}
	}

	//update camera
	playerCam->update(deltaTime);

	//Step physics simulation last
	bulletHandler->update(deltaTime);

	return true;
}


bool Game::frameEnded(const Ogre::FrameEvent &evt)
{
	return true;
}

void Game::setupLights()
{
	//AMBIENT
    //Setup Ambient light
    scnMgr->setAmbientLight(ColourValue(0.1, 0.1, 0.1));
    scnMgr->setShadowTechnique(ShadowTechnique::SHADOWTYPE_STENCIL_MODULATIVE);

	//DIRECTIONAL
    //Create directional light
    Light* directionalLight = scnMgr->createLight("DirectionalLight");

    //Configure the light
    directionalLight->setType(Light::LT_DIRECTIONAL);
    directionalLight->setDiffuseColour(ColourValue(0.5, 0.5, 0.5));
    directionalLight->setSpecularColour(ColourValue(0.5, 0.5, 0.5));

    //Setup a scene node for the directional lightnode.
    SceneNode* directionalLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();
    directionalLightNode->attachObject(directionalLight);
    directionalLightNode->setDirection(Vector3(0, -1, 1));


	//Create another directional light
	Light* directionalLight2 = scnMgr->createLight("DirectionalLight2");

	directionalLight2->setType(Light::LT_DIRECTIONAL);
	directionalLight2->setDiffuseColour(ColourValue(0.5, 0.5, 0.5));
	directionalLight2->setSpecularColour(ColourValue(0.5, 0.5, 0.5));

	SceneNode* directionalLight2Node = scnMgr->getRootSceneNode()->createChildSceneNode();
	directionalLight2Node->attachObject(directionalLight2);
	directionalLight2Node->setDirection(Vector3(0.5, 0.9, 0.7));



	////POINT
 //   //Create a point light
 //   Light* pointLight = scnMgr->createLight("PointLight");

 //   //Configure the light
 //   pointLight->setType(Light::LT_POINT);
 //   pointLight->setDiffuseColour(0.3, 0.3, 0.3);
 //   pointLight->setSpecularColour(0.3, 0.3, 0.3);

 //   //setup the scene node for the point light
 //   SceneNode* pointLightNode = scnMgr->getRootSceneNode()->createChildSceneNode();

 //   //Configure the light
 //   pointLightNode->setPosition(Vector3(0, 150, 250));

 //   //Add the light to the scene.
 //   pointLightNode->attachObject(pointLight);
}

bool Game::keyPressed(const KeyboardEvent& evt)
{
    if (evt.keysym.sym == SDLK_ESCAPE)
    {
        getRoot()->queueEndRendering();
    }
	//key pressed, so set keystates
	switch (evt.keysym.sym) {

	case 'w':
		keys |= Keys::Forward;
		break;
	case 'a':
		keys |= Keys::Left;
		break;
	case 's':
		keys |= Keys::Backward;
		break;
	case 'd':
		keys |= Keys::Right;
		break;
	case 'r':
		keys |= Keys::Up;
		break;
	case 'f':
		keys |= Keys::Down;
		break;
	case 'q':
		keys |= Keys::RollL;
		break;
	case 'e':
		keys |= Keys::RollR;
		break;
	default:
		break;
	}
	//Give the player the new keystates
	player->updateKeystate(keys);

	return true;
}

bool Game::keyReleased(const KeyboardEvent& evt) {

	//Key released, so set reset keystates
	switch (evt.keysym.sym) {

	case 'w':
		keys &= ~Keys::Forward;
		break;
	case 'a':
		keys &= ~Keys::Left;
		break;
	case 's':
		keys &= ~Keys::Backward;
		break;
	case 'd':
		keys &= ~Keys::Right;
		break;
	case 'r':
		keys &= ~Keys::Up;
		break;
	case 'f':
		keys &= ~Keys::Down;
		break;
	case 'q':
		keys &= ~Keys::RollL;
		break;
	case 'e':
		keys &= ~Keys::RollR;
		break;
	default:
		break;
	}
	//Give player the new keystates
	player->updateKeystate(keys);
	return true;
}

bool Game::mouseMoved(const MouseMotionEvent& evt)
{
	//evt.xrel and yrel return how much the mouse moved since the last function call
	//we then pass that to our camera

	int centreW = (int)getRenderWindow()->getWidth() / 2;
	int centreH = (int)getRenderWindow()->getHeight() / 2;
	playerCam->setMovedDist(evt.xrel, evt.yrel);


	//Using windows functions I reset the mouse to the centre of the window
	//SetCursorPos((int)getRenderWindow()->getWidth() / 2, (int)getRenderWindow()->getHeight() / 2);

	return true;
}
