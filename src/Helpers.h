#pragma once
#include "Ogre.h"

#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"

using namespace Ogre;

Quaternion genQuaternion(Radian, Radian, Radian);

btQuaternion ogreQuatToBT(Quaternion);

btVector3 vec3ToBT(Vector3);