#pragma once
#include <stdint.h>

typedef uint32_t Keystate;

enum Keys {

	Forward = 0b00000001,
	Backward = 0b00000010,
	Left = 0b00000100,
	Right = 0b00001000,
	Up = 0b00010000,
	Down = 0b00100000,
	RollR = 0b01000000,
	RollL = 0b10000000,

};
