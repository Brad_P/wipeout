#pragma once

#define _USE_MATH_DEFINES
//System
#include <iostream>
#include <Windows.h>
#include <random>
#include <math.h>

//Ogre
#include "Ogre.h"
#include "OgreApplicationContext.h"
#include "OgreInput.h"
#include "OgreRTShaderSystem.h"

//Ogre-Bullet helpers
#include "BtOgreExtras.h"
#include "BtOgreGP.h"
#include "BtOgrePG.h"

//Bullet
#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"

//My project
#include "BulletHandler.h"
#include "ShipFactory.h"
#include "Keystates.h"
#include "Helpers.h"

#include "World.h"
#include "Player.h"
#include "NPCRacer.h"
#include "PlayerCamera.h"




using namespace Ogre;
using namespace OgreBites;
using namespace btOgre;


class Game : public ApplicationContext, public InputListener
{
public:
	Game();
	virtual ~Game();

	void setup();

	bool keyPressed(const KeyboardEvent& evt);
	bool keyReleased(const KeyboardEvent& evt);
	bool mouseMoved(const MouseMotionEvent& evt);

	bool frameStarted (const FrameEvent &evt);
	bool frameEnded(const FrameEvent &evt);


private:

	SceneManager * scnMgr;
	BulletHandler* bulletHandler;
	RenderWindow* mainWindow;
	Keystate keys = 0;

	ShipFactory* shipFactory;
	std::vector<Ship*> racers;
	const std::vector<Vector3> START_POSITIONS = {Vector3(-290,10,0), Vector3(-260,10,0), 
														Vector3(-275,10,50), 
												  Vector3(-290,10,100), Vector3(-260,10,100), 
														Vector3(-275,10,150)
	};

	//SETUP FUNCTIONS
    void setupBullet();
	void setupShipFactory();

	void setupWorld();
	void setupPlayer();
	void setupRacers();

	void setupTrack();

	void setupLights();
	void setupCam();



	World* world; 
	Player* player;
	PlayerCamera* playerCam;
};
