#pragma once

#include <iostream>

#include "Ogre.h"

using namespace Ogre;

class PlayerCamera {

public:

	PlayerCamera(SceneNode*, SceneNode*);
	~PlayerCamera();

	void initViewport(RenderWindow*, Camera*);
	void setupCamera(SceneManager*);


	void setMovedDist(int x, int y);
	void update(float);

	SceneNode* getCameraNode() { return mCameraNode; }


private:

	SceneNode* playerSceneNode;

	SceneNode * mCameraPivot;
	SceneNode * mCameraGoal;
	SceneNode * mCameraNode;

	//Most recent amount that the cursor moved
	int xMoved = 0;
	int yMoved = 0;

	//camera sensitivity modifier
	float sensX = 0.5f;
	float sensY = 0.5f;

	float mPivotPitch = 0.0f;

};