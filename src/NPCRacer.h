#pragma once

//System
#include <iostream>

//Ogre

//Bullet

//Project
#include "Ship.h"
#include "BulletHandler.h"
#include "Helpers.h"

class NPCRacer : public Ship {


public:

	NPCRacer(SceneNode*, btRigidBody*, BulletHandler*);
	~NPCRacer();

	void update(float);

	SceneNode* getSceneNode() { return mySceneNode; }
	btRigidBody* getRigidBody() { return myRigidBody; }
	void setPosition(btVector3, btQuaternion);

private:

	bool isAtNode(Vector3, Vector3);

	BulletHandler* bulletHandler;

	SceneNode* mySceneNode;
	btRigidBody* myRigidBody;

	//Positions of all the nodes it needs to follow
	const std::vector<Vector3> TRACK_PATH_POSITIONS = {
	Vector3(-290, 10, -307),
	Vector3(-287, 15, -715),
	Vector3(-170, 25, -925),
	Vector3(110, 75, -885),
	Vector3(298, 80, -774),
	Vector3(362, 35, -460),
	Vector3(333, 5, -185),
	Vector3(587, -15, -38),
	Vector3(538, 10, 250),
	Vector3(404, 30, 615),
	Vector3(250, 40, 740),
	Vector3(-115, -5, 784),
	Vector3(-223, -5, 700),
	Vector3(-260, 10, 415),
	Vector3(-288, 10, 304),
	Vector3(-277, 10, 0)
	};

	const int NODE_HIT_RADIUS = 15;
	int nodeIndex = 0;
	Vector3 targetNode;

	float speed = 1.4f;
};
