#pragma once

#include "PlayerCamera.h"

//The camera node knows about its own node and its target's node
PlayerCamera::PlayerCamera(SceneNode* scnNode, SceneNode* playerScnNode)
{

	mCameraNode = scnNode;
	playerSceneNode = playerScnNode;
}

PlayerCamera::~PlayerCamera()
{
}

void PlayerCamera::initViewport(RenderWindow* rw, Camera* cam) {

	// Setup viewport for the camera.
	Viewport* vp = rw->addViewport(cam);
	vp->setBackgroundColour(ColourValue(0, 0, 0));

	//Set the aspect ratio of the camera based on the viewport's dimensions
	cam->setAspectRatio(Real(1920) / Real(1080));

	cam->setNearClipDistance(0.1);

}


void PlayerCamera::setupCamera(SceneManager* scMgr)
{
	// create a pivot at the player's 'CoM'
	mCameraPivot = scMgr->getRootSceneNode()->createChildSceneNode();
	// this is where the camera should be soon, and it spins around the pivot
	mCameraGoal = mCameraPivot->createChildSceneNode(Vector3(0, 0, 100));
	// this is where the camera actually is
	mCameraNode->setPosition(mCameraPivot->getPosition() + mCameraGoal->getPosition());

	mCameraPivot->setFixedYawAxis(true);
	mCameraGoal->setFixedYawAxis(true);
	mCameraNode->setFixedYawAxis(true);

	mPivotPitch = 0;
}

void PlayerCamera::update(float deltaTime)
{

	// place the camera pivot roughly at the character's shoulder
	mCameraPivot->setPosition(playerSceneNode->getPosition());
	// move the camera smoothly to the goal
	Vector3 goalOffset = mCameraGoal->_getDerivedPosition() - mCameraNode->getPosition();
	mCameraNode->translate(goalOffset);
	// always look at the pivot
	mCameraNode->lookAt(mCameraPivot->_getDerivedPosition(), Node::TS_PARENT);

}

//pass in the amount the mouse moved to apply rotation to the pivot
void PlayerCamera::setMovedDist(int deltaYaw, int deltaPitch)
{
	int deltaZoom = 0;

	deltaPitch = -deltaPitch;
	mCameraPivot->yaw(Degree(-deltaYaw) * sensX, Node::TS_PARENT);

	// bound the pitch
	if (!(mPivotPitch + deltaPitch > 50 && deltaPitch > 0) &&
		!(mPivotPitch + deltaPitch < -80 && deltaPitch < 0))
	{
		mCameraPivot->pitch(Degree(deltaPitch) * sensY, Node::TS_LOCAL);
		mPivotPitch += deltaPitch;
	}

	Real dist = mCameraGoal->_getDerivedPosition().distance(mCameraPivot->_getDerivedPosition());
	Real distChange = deltaZoom * dist;

	// bound the zoom
	if (!(dist + distChange < 8 && distChange < 0) &&
		!(dist + distChange > 25 && distChange > 0))
	{
		mCameraGoal->translate(0, 0, distChange, Node::TS_LOCAL);
	}
}